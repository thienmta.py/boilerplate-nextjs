module.exports = {
  parserPreset: "conventional-changelog-conventionalcommits",
  rules: {
    "body-leading-blank": [1, "always"],
    "body-max-line-length": [2, "always", 200],
    "footer-leading-blank": [1, "always"],
    "footer-max-line-length": [2, "always", 200],
    "header-max-length": [2, "always", 200],
    // "scope-enum": [2, "always", ["auto-test", "common", "dev"]],
    "subject-case": [
      2,
      "never",
      ["sentence-case", "start-case", "pascal-case", "upper-case"],
    ],
    "subject-empty": [2, "never"],
    "subject-full-stop": [2, "never", "."],
    "type-case": [2, "always", "lower-case"],
    "type-empty": [2, "never"],
    "type-enum": [
      2,
      "always",
      [
        "build",
        "test",
        "docs",

        "feature",
        "feedback",
        "revert",

        "style",
        "lint",
        "refactor",
      ],
    ],
  },
};
