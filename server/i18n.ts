// #region Global Imports
import NextI18Next from "next-i18next";
// import { getStaticUri } from "../src/Services/Helpers/path-builder";
// #endregion Global Imports

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: "jp",
  otherLanguages: ["en", "vi"],
  // backend: {
  //   loadPath: getStaticUri(`/static/locales/{{lng}}/{{ns}}.json`),
  // },
});

export const { appWithTranslation, withTranslation } = NextI18NextInstance;

export default NextI18NextInstance;
