// #region Global Imports
const nextRoutes = require("next-routes");
// #endregion Global Imports

function createRouter(basePath?: string) {
  basePath = basePath || "/";
  const routes = (module.exports = nextRoutes());
  routes.add("home", basePath);
  routes.add("/404", `${basePath}/404`);

  return routes;
}

export default createRouter;
