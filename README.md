# <div align="center">Portal Site <i>(pms-spa-portal)</i></div>

## About

Source code SPA của module pms-spa-portal  
Base stacks: Nextjs, Redux-Thunk, TypeScript, Express.js, Styled-Component

## Features

* **Next.js** - Minimalistic framework for server-rendered React applications.
* **Typescript** - Superset of JavaScript which primarily provides optional static typing, classes and interfaces.
* **Redux** - A predictable state container for JavaScript apps.
* **Express.js**- A minimal and flexible Node.js web application framework that handles server-side rendering and integrates with Next.js.
* **Sass/Scss** - CSS preprocessor, which adds special features such as variables, nested rules and mixins (sometimes referred to as syntactic sugar) into regular CSS.
* **Docker** - A tool designed to make it easier to create, deploy, and run applications by using containers.
* **Babel** -  The compiler for next generation JavaScript.
* **ESLint** - The pluggable linting utility.
* **Reverse Proxy** - Lightweight server for proxying API requests.
* **Bundler Analyzer** - Visualize the size of webpack output files with an interactive zoomable treemap.
* **Jest** - Javascript testing framework , created by developers who created React.
* **React Testing Library** - Simple and complete React DOM testing utilities that encourage good testing practices.
* **next-runtime-dotenv** - Expose environment variables to the runtime config of Next.js
* **next-i18next** - An internationalization-framework which provides a function that takes a key, some options, and returns the value for the current language. Helps you to add language translation support to your app.
* **Storybook** - An open source tool for developing UI components in isolation for React.

## Setup & Documentation

```sh
# 1st step, install dependencies
$ npm install  

# local run
$ npm run dev  

# start storybook server
$ npm run storybook

# test
$ npm run test
$ npm run test-no-cache
```
