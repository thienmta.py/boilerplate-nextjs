FROM node:12-alpine
ENV NODE_ENV production


COPY package*.json /tmp/

RUN cd /tmp && npm install
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/


WORKDIR /opt/app
COPY . /opt/app

RUN npm ci
RUN npm install --only=dev
RUN npm run build

CMD [ "npm", "start" ]
