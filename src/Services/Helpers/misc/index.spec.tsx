import { cloneDeep, isEmpty } from "@Services/Helpers/misc";

describe("Services", () => {
  describe("Helpers", () => {
    describe("misc", () => {
      it("cloneDeep function", () => {
        const obj = {
          "1": 1,
          "2": 2,
        }; // IEpisodeModel
        const result = {
          "1": 1,
          "2": 2,
        };
        expect(cloneDeep(obj)).toEqual(result);
      });

      it("isEmpty function object isn't empty", () => {
        const obj = {
          "1": 1,
          "2": 2,
        };

        expect(isEmpty(obj)).toEqual(false);
      });

      it("isEmpty function object is empty", () => {
        const obj = {};

        expect(isEmpty(obj)).toEqual(true);
      });

      it("isEmpty function array is empty", () => {
        const arr: any[] = [];

        expect(isEmpty(arr)).toEqual(true);
      });

      it("isEmpty function array isn't empty", () => {
        const obj = [1, 2];

        expect(isEmpty(obj)).toEqual(false);
      });
    });
  });
});
