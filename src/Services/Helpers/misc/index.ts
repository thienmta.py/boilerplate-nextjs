export function cloneDeep<T>(a: T): T {
  return JSON.parse(JSON.stringify(a));
}

/**
 *
 * @param obj Array or Object
 *
 * return true of the param is passed is empty otherwise return false
 *
 * isEmpty()             => false
 * isEmpty({})           => true
 * isEmpty({a: "1"})     => false
 * isEmpty([1, 2, 3, 4]) => false
 *
 */
export function isEmpty(obj: any[] | object): boolean {
  if (!obj) return false;

  if (Array.isArray(obj)) {
    return !obj.length;
  }

  return !Object.keys(obj).length;
}

export function isMobile() {
  const toMatch = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
  ];

  return toMatch.some((toMatchItem) => {
    return navigator.userAgent.match(toMatchItem);
  });
}

export function iOS() {
  return (
    [
      "iPad Simulator",
      "iPhone Simulator",
      "iPod Simulator",
      "iPad",
      "iPhone",
      "iPod",
    ].includes(navigator.platform) ||
    // iPad on iOS 13 detection
    (navigator.userAgent.includes("Mac") && "ontouchend" in document)
  );
}
