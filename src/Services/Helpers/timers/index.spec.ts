import { toHHMMSS } from "@Services/Helpers/timers";

describe("Services>Helper>timers", () => {
  test("test toHHMMSS params is empty", () => {
    const result = toHHMMSS();
    expect(result).toEqual("00:00:00");
  });

  test("test toHHMMSS params is null", () => {
    const result = toHHMMSS(null);
    expect(result).toEqual("00:00:00");
  });

  test("test toHHMMSS params is '' ", () => {
    const result = toHHMMSS("");
    expect(result).toEqual("00:00:00");
  });

  test("test toHHMMSS params is acvbvcb", () => {
    // @ts-ignore
    const result = toHHMMSS("acvbvcb");
    expect(result).toEqual("00:00:00");
  });

  test("test toHHMMSS params 1", () => {
    const result = toHHMMSS(1);
    expect(result).toEqual("00:01");
  });

  test("test toHHMMSS params 10", () => {
    const result = toHHMMSS(10);
    expect(result).toEqual("00:10");
  });

  test("test toHHMMSS params 100", () => {
    const result = toHHMMSS(100);
    expect(result).toEqual("01:40");
  });

  test("test toHHMMSS params 1000", () => {
    const result = toHHMMSS(1000);
    expect(result).toEqual("16:40");
  });

  test("test toHHMMSS params 10000", () => {
    const result = toHHMMSS(10000);
    expect(result).toEqual("02:46:40");
  });
});
