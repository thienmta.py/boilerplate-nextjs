/* eslint-disable */
/**
 *
 * @param timeInSecound
 *
 * toHHMMSS()          => 00:00:00
 * toHHMMSS(null)      => 00:00:00
 * toHHMMSS(undefined) => 00:00:00
 * toHHMMSS("")        => 00:00:00
 * toHHMMSS("acvbvcb") => 00:00:00
 * toHHMMSS(1)         => "00:01"
 * toHHMMSS(10)        => "00:10"
 * toHHMMSS(100)       => "01:40"
 * toHHMMSS(1000)      => "16:40"
 * toHHMMSS(10000)     => "02:46:40"
 *
 */
export const toHHMMSS = (timeInSecound?: number | null | undefined | "") => {
  if (!timeInSecound) return "00:00:00";

  const sec_num = parseInt(String(timeInSecound), 10);

  if (isNaN(sec_num)) return "00:00:00";

  const hours = Math.floor(sec_num / 3600);
  const minutes = Math.floor(sec_num / 60) % 60;
  const seconds = sec_num % 60;

  return [hours, minutes, seconds]
    .map((v) => (v < 10 ? `0${v}` : v))
    .filter((v, i) => v !== "00" || i > 0)
    .join(":");
};
