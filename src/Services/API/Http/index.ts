import "isomorphic-unfetch";
import getConfig from "next/config";
import { stringify } from "query-string";
import { HttpModel } from "@Services/API/Http/Http";

const {
  publicRuntimeConfig: {
    API_KEY,
    // API_URL
  },
} = getConfig() || { publicRuntimeConfig: {} };

/**
 * demo base URL remove if unnecessary
 */
const BaseUrl = "https://jsonplaceholder.typicode.com";
/**
 * uncomment the line below
 */
// const BaseUrl = `${API_URL}`;

export const handleHTTPError = (e: any) => {
  console.error("ERROR::Error in Http Service");
  console.error(e);
};

export const Http = {
  Request: async <A>(
    methodType: string,
    url: string,
    args?: HttpModel.IRequest
  ): Promise<A> => {
    const { params, body, transform } = (args as any) || {};
    // @ts-ignore
    return new Promise((resolve, reject) => {
      const query = params
        ? `?${stringify({ ...params, api_key: API_KEY })}`
        : "";

      fetch(`${BaseUrl}${url}${query}`, {
        body: JSON.stringify(body),
        cache: "no-cache",
        // mode: "no-cors",
        headers: {
          "content-type": "application/json",
        },
        method: `${methodType}`,
      })
        .then(async (response) => {
          if (response.status === 200) {
            try {
              return response.json();
            } catch (e) {
              handleHTTPError(e);
              resolve();
            }
          }
          return reject(response);
        })
        .then(async (response) => {
          if (transform) {
            try {
              resolve(transform(response));
            } catch (e) {
              handleHTTPError(e);
              resolve();
            }
          }
          resolve(response);
        });
    }).catch((e) => {
      handleHTTPError(e);
      return {};
    });
  },
};
