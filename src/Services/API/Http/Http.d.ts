declare namespace HttpModel {
  export interface IRequestQueryPayload {
    [key: string]: {};
  }

  export interface IRequest {
    params?: any;
    body?: IRequestQueryPayload;
    transform?: void;
  }

  export interface IResponse {
    data?: Array<>;
    content_data?: object;

    page?: number;
    limit?: number;
    total_count?: number;
    total_count?: number;

    response_code: number;
    response_msg: string;
  }
}

export { HttpModel };
