import { ActionConst } from "@Definitions";
import { HomeReducer, INITIAL_STATE } from "@Redux/Reducers/home";

describe("Redux>Reducers>home", () => {
  it("should handle undefined action", () => {
    expect(
      HomeReducer(INITIAL_STATE, {
        type: undefined,
        payload: { data: [] },
      })
    ).toMatchObject(INITIAL_STATE);
  });

  it("handle action SetPickupData", () => {
    expect(
      HomeReducer(INITIAL_STATE, {
        type: ActionConst.Home.SetPickupData,
        payload: { data: [] },
      })
    ).toMatchObject(INITIAL_STATE);
  });

  it("handle action SetNewsData", () => {
    expect(
      HomeReducer(INITIAL_STATE, {
        type: ActionConst.Home.SetNewsData,
        payload: { data: [] },
      })
    ).toMatchObject(INITIAL_STATE);
  });

  it("handle action SetDocumentaryData", () => {
    expect(
      HomeReducer(INITIAL_STATE, {
        type: ActionConst.Home.SetDocumentaryData,
        payload: { data: [] },
      })
    ).toMatchObject(INITIAL_STATE);
  });

  it("handle action SetSpecialData", () => {
    const data = HomeReducer(INITIAL_STATE, {
      type: ActionConst.Home.SetSpecialData,
      payload: { content_data: { content_title: "" }, data: [] },
      params: {
        position: "T",
      },
    });
    expect(data).toMatchObject(INITIAL_STATE);
  });
});
