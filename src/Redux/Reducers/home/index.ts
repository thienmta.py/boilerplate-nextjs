import { produce } from "immer";
import { ActionConst } from "@Definitions";
import { IActionAPI } from "@Redux/Actions";

export interface ContentRankingDataInterface {
  update_time?: String;
}

export type IHomeReducer = {
  data?: Array<any>;
};

export const INITIAL_STATE: IHomeReducer = {
  data: [],
};

export const HomeReducer = (state = INITIAL_STATE, action: IActionAPI) => {
  return produce(state, (draft) => {
    if (!action.payload || !action.payload.data) {
      return;
    }

    switch (action.type) {
      case ActionConst.Home.SetDemoAPI:
      case ActionConst.Home.GetDemoAPI: {
        if (Array.isArray(action.payload.data)) {
          draft.data = action.payload.data;
        }
        break;
      }
      default:
        break;
    }
  });
};
