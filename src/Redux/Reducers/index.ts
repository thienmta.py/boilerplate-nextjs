// #region Global Imports
import { combineReducers } from "redux";
// #endregion Global Imports

// #region Local Imports
import { HomeReducer } from "@Redux/Reducers/home";
// #endregion Local Imports

export default combineReducers({
  home: HomeReducer,
});
