import { Dispatch } from "redux";
import { ActionConst } from "@Definitions";
import { Http } from "@Services";
import { apiConstant } from "@Definitions/APIEndpoint";
import { IRespondData } from "@Definitions/Interfaces/models/IRespondData";

export const HomeActions = {
  GetDemoApi: () => async (dispatch: Dispatch) => {
    const result = await Http.Request<IRespondData>(
      "GET",
      apiConstant.API_GET_TEST
    );

    // result will be list videos (in expected)
    dispatch({
      payload: result,
      type: ActionConst.Home.GetDemoAPI,
    });
  },
  SetDemoApi: (data: any) => (dispatch: Dispatch) => {
    dispatch({
      payload: data,
      type: ActionConst.Home.SetDemoAPI,
    });
  },
};
