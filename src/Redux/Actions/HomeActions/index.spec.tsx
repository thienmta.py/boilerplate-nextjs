import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { HomeActions } from "@Redux/Actions";
import { ActionConst } from "@Definitions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("Redux>Actions>HomeActions", () => {
  test("GetPickupData", async () => {
    const store = mockStore({});

    const expectedActions = [
      {
        payload: expect.arrayContaining([]),
        type: ActionConst.Home.GetDemoAPI,
      },
    ];

    await store.dispatch<any>(HomeActions.GetDemoApi());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
