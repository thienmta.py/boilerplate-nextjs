import { Action as ReduxAction } from "redux";
import { HttpModel } from "@Services/API/Http/Http";

export { HomeActions } from "./HomeActions";

export interface IActionAPI<>extends ReduxAction {
  payload?: HttpModel.IResponse & {};
  params?: any;
  options?: any;
}

export interface IActionClient extends ReduxAction {
  payload?: any;
}
