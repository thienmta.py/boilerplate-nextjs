// #region Interface Imports
import { IHomeReducer } from "@Redux/Reducers/home";
// #endregion Interface Imports

export interface IStore {
  home: IHomeReducer;
}
