import { ActionConst } from ".";

describe("Action constants", () => {
  describe("Home", () => {
    it("should have Home__GetDemoAPI", () => {
      expect(ActionConst.Home.GetDemoAPI).toBe("Home__GetDemoAPI");
    });

    it("should have Home__SetDemoAPI", () => {
      expect(ActionConst.Home.SetDemoAPI).toBe("Home__SetDemoAPI");
    });
  });
});
