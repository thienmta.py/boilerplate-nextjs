// #region Page Interfaces
import { WithTranslation } from "next-i18next";
import { NextPage } from "next";

export * from "@Redux/IStore";
export * from "@Definitions/Interfaces/pages/_app";
export * from "@Definitions/Interfaces/pages/_error";

export interface INextPageProps extends WithTranslation {}
export interface INextPageInitialProps {
  namespacesRequired: string[];
}

export type NextPageType<P = {}, IP = P> = NextPage<
  P & INextPageProps,
  IP & INextPageInitialProps
>;
