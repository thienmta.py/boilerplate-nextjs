/**
 * remove if unnecessary
 */
export interface IRespondData {
  content_data?: string | object | null;
  data: Array<any>;
  limit?: number;
  page?: number;
  respone_code?: number;
  respone_msg?: string | null;
  total_count?: number;
}
