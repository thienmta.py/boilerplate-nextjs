/**
 * remove if unnecessary
 */
export const colorConstants = {
  PURPLE: {
    main: "linear-gradient(273.86deg, #78489E 30.62%, #683D88 100%)",
    side: "rgba(104, 61, 136, 0.7)",
  },
  GREEN: {
    main: "linear-gradient(91.55deg, #0B7467 8.2%, #0D8677 84.78%)",
    side: "rgba(11, 116, 103, 0.7)",
  },
  BLUE: {
    main: "linear-gradient(91.55deg, #045B8B 8.2%, #05699F 84.67%)",
    side: "rgba(4, 91, 139, 0.7)",
  },
  ORANGE: {
    main: "linear-gradient(91.55deg, #CD721E 8.2%, #E1832B 84.78%)",
    side: "rgba(205, 114, 30, 0.7)",
  },
  PINK: {
    main: "linear-gradient(91.55deg, #BD557B 8.2%, #C65E83 84.78%)",
    side: "rgba(189, 85, 123, 0.7)",
  },
};
