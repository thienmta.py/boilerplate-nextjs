import { Theme } from "@material-ui/core";

export interface IAppTheme extends Theme {
  colors?: any;
  customColor?: any;
  typography: any;
  skeletonItem?: any;
}

const appTheme = {
  spacing: 8,
  colors: {
    primary: "#BC0C16",
  },
  customColor: {
    WHITE: "#FFFFFF",
    MENU_WRAPPER: "#EAEAEA",
    DATE_COLOR: "rgba(0, 0, 0, .4)",
    BLACK: "#000000",
    GRAY_00: "#F1F1F1",
    GRAY_01: "#DADADA",
    GRAY_02: "#C4C4C4",
    GRAY_03: "#DDDDDD",
    BLUE_01: "#002B60",
    BLUE_02: "#00234F",
    YELLOW_00: "#F7DC6F",
  },
  typography: {
    htmlFontSize: 16,
    h1: {}, // 96
    h2: {}, // 60
    h3: {}, // 48
    h4: {}, // 34
    h5: {
      fontSize: "1.625rem", // 26
    },
    h6: {}, // 20
    bigTitle: {
      fontSize: "1.125rem",
    },
    subtitle1: {
      fontSize: "1rem",
    }, // 16
    subtitle2: {
      fontSize: "0.875rem",
    }, // 14
    body1: {
      fontSize: "1rem",
    }, // 16
    body2: {
      fontSize: "0.875rem",
    }, // 14
    button: {
      fontSize: "0.875rem",
    }, // 14
    smaller: {
      fontSize: "0.875rem",
    },
    caption: {
      fontSize: "0.75rem",
    }, // 12
    overline: {
      fontSize: "0.625rem",
    }, // 10
    index: {
      fontSize: "0.75rem",
    },
    textLeft: "left",
    textCenter: "center",
  },
  breakpoints: {
    values: {
      xs: 375,
      xsm: 425,
      sm: 768,
      md: 996,
      lg: 1200,
      xl: 1374,
    },
  },
  overrides: {
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: "#EAEAEA",
      },
    },
  },
  skeletonItem: {
    round: "5px",
  },
};

export { appTheme };
