import React from "react";
import { Layout } from "@Components/Layout";

export default {
  title: "Layout",
};

export const MainLayout = () => (
  <Layout>
    <div style={{ background: "red" }}>Main Content</div>
  </Layout>
);
