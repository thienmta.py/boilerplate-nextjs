import * as React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Grid } from "@material-ui/core";
import { IAppTheme } from "@Definitions/Styled/theme";
import { LayoutProps } from "./Layout";

const useStyles = makeStyles((theme: IAppTheme) => ({
  gridLayout: {
    margin: theme.spacing(-5) / 2,
    width: "inherit",
  },
  layoutSection: {
    padding: "20px 0",
    backgroundColor: "#fff",
  },
  spacing_5: {
    padding: theme.spacing(5) / 2,
  },
}));

const Layout: React.FunctionComponent<LayoutProps> = ({
  children,
}): JSX.Element => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classNames(classes.layoutSection)}>
        <main>
          <Grid container className={classNames(classes.gridLayout)}>
            <Grid
              item
              className={classNames(classes.spacing_5)}
              xl={12}
              md={12}
              sm={12}
              xs={12}
            >
              {children}
            </Grid>
          </Grid>
        </main>
      </div>
    </React.Fragment>
  );
};

export { Layout };
