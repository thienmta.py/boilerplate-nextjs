import React from "react";
import { FigmaIcons } from "@Components/Icons";

export default {
  title: "Components/Icons",
};

export const AllIcon = () => (
  <div>
    <FigmaIcons.Error />
    <FigmaIcons.EmptyBox />
  </div>
);
