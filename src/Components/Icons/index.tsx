import React from "react";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import * as FigmaIcons from "./FigmaIcons";

export * from "@material-ui/icons";
export { FigmaIcons };

export interface IIcon {
  /**
   * Node passed into the SVG element.
   */
  children?: React.ReactNode;
  /**
   * The color of the component. It supports those theme colors that make sense for this component.
   * You can use the `htmlColor` prop to apply a color attribute to the SVG element.
   */
  color?: "inherit" | "primary" | "secondary" | "action" | "disabled" | "error";
  /**
   * The fontSize applied to the icon. Defaults to 24px, but can be configure to inherit font size.
   */
  fontSize?: "inherit" | "default" | "small" | "large";
  /**
   * Applies a color attribute to the SVG element.
   */
  htmlColor?: string;
  /**
   * The shape-rendering attribute. The behavior of the different options is described on the
   * [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/shape-rendering).
   * If you are having issues with blurry icons you should investigate this prop.
   * @document
   */
  shapeRendering?: string;
  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess?: string;
  /**
   * Allows you to redefine what the coordinates without units mean inside an SVG element.
   * For example, if the SVG element is 500 (width) by 200 (height), and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the SVG will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox?: string;
  className?: any;
}

export interface ICustomeIcon {
  color?: string;
  fontSize?: number | string; // unit must be rem if its type is number
  opacity?: string | number;
  width?: number;
  height?: number;
  style?: CSSProperties;
  className?: any;
  wrapperClassName?: string;
  wrapperStyles?: CSSProperties;
  textStyles?: CSSProperties;
  textClassName?: string;
}
