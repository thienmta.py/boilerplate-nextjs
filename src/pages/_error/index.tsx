// #region Global Imports
import * as React from "react";
import { NextPage } from "next";
import Link from "next/link";
import { withTranslation } from "@Server/i18n";
import { IErrorPage } from "@Definitions/Interfaces/pages/_error";
import { Grid } from "@material-ui/core";
import { FigmaIcons } from "@Components";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { IAppTheme } from "@Definitions/Styled";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import { routerNames } from "@Definitions/RouterNames";

const useStyles = makeStyles((theme: IAppTheme) => {
  return {
    boxError: {
      [theme.breakpoints.up("sm")]: {
        flexDirection: "row-reverse",
        marginLeft: "-70px",
        textAlign: "left",
      },
      textAlign: "center",
      rowGap: "20px",
      width: "auto",
      boxSizing: "content-box",
      "& > *": {
        [theme.breakpoints.up("sm")]: {
          paddingLeft: "70px !important",
        },
        [theme.breakpoints.down("xs")]: {
          width: "100%",
        },
      },
    },
    sectionLarge: {
      backgroundColor: "#fff",
    },
    boxBackhome: {
      marginTop: "54px",
      fontSize: "14px",
      fontWeight: "bold",
      "& a": {
        color: "#333",
      },
    },
    boxError__txt1: {
      fontSize: "15px",
      lineHeight: "1.2",
      letterSpacing: "0.03em",
      color: "#999",
      marginBottom: "12px",
      fontWeight: "bold",
    },
    boxError__txt2: {
      fontSize: "16px",
      lineHeight: "1.62",
      fontWeight: "bold",
      marginBottom: "6px",
      color: "#333",
    },
    boxError__txt3: {
      fontSize: "14px",
      lineHeight: "1.57",
      color: "#333",
    },
  };
});

const Error: NextPage<IErrorPage.IProps, IErrorPage.InitialProps> = () => {
  const classes = useStyles();

  return (
    <Box className={classNames(classes.sectionLarge)}>
      <Container maxWidth="lg">
        <Grid
          container
          alignItems="center"
          className={classNames(classes.boxError)}
        >
          <Grid item>
            <FigmaIcons.Error />
          </Grid>
          <Grid item sm>
            <Typography className={classes.boxError__txt1}>
              Error Page
            </Typography>
            <Typography className={classes.boxError__txt2}>
              お探しのページは見つかりませんでした。
            </Typography>
            <Typography className={classes.boxError__txt3}>
              申し訳ございません。URLが正しく入力されているか、確認してください。
            </Typography>
            <Typography className={classes.boxBackhome}>
              <Link href={routerNames.HOME}>
                <span style={{ cursor: "pointer" }}>トップページ </span>
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

Error.getInitialProps = async ({ res, err }) => {
  let statusCode;

  if (res) {
    ({ statusCode } = res);
  } else if (err) {
    ({ statusCode } = err);
  }

  return {
    namespacesRequired: ["common"],
    statusCode,
  };
};

export default withTranslation("common")(Error);
