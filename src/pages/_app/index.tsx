import * as React from "react";
import App, { AppInitialProps, AppContext } from "next/app";
import Head from "next/head";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
import {
  createMuiTheme,
  ThemeProvider as MUIThemeProvider,
  createGenerateClassName,
  StylesProvider,
} from "@material-ui/core/styles";
import withRedux from "next-redux-wrapper";
import { appTheme } from "@Definitions/Styled";
import { appWithTranslation } from "@Server/i18n";
import { makeStore } from "@Redux";
import { Layout } from "@Components";
import { AppWithStore } from "@Definitions/Interfaces/pages/_app";
import "@Static/css/main.scss";

class WebApp extends App<AppWithStore> {
  static async getInitialProps({
    Component,
    ctx,
  }: AppContext): Promise<AppInitialProps> {
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};

    return { pageProps };
  }

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      // @ts-ignore
      jssStyles.parentNode.removeChild(jssStyles);
    }
    const bodyEl = document.querySelector("body");
    if (bodyEl) {
      bodyEl.style.backgroundColor = "#fff";
    }
  }

  render() {
    const { Component, pageProps, store } = this.props;
    const generateClassName = createGenerateClassName({
      disableGlobal: false,
    });

    return (
      <StylesProvider generateClassName={generateClassName}>
        <Head>
          <title>Next Web App</title>
        </Head>
        <Provider store={store}>
          <MUIThemeProvider theme={createMuiTheme(appTheme)}>
            <ThemeProvider theme={appTheme}>
              <Layout {...Component}>
                <Component {...pageProps} />
              </Layout>
            </ThemeProvider>
          </MUIThemeProvider>
        </Provider>
      </StylesProvider>
    );
  }
}

export default withRedux(makeStore)(appWithTranslation(WebApp));
