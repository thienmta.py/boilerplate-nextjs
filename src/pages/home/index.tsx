import React, { useEffect, useState } from "react";
import Head from "next/head";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import { withTranslation } from "@Server/i18n";
import { IAppTheme } from "@Definitions/Styled";
import {
  INextPageInitialProps,
  NextPageType,
} from "@Definitions/Interfaces/pages";
import { Http } from "@Services";
import { apiConstant } from "@Definitions/APIEndpoint";

const useStyles = makeStyles((theme: IAppTheme) => ({
  uppercase: {
    textTransform: "uppercase",
  },
  blue_01: {
    color: theme.customColor.BLUE_01,
  },
}));

interface IPost {
  userId?: number;
  id?: number;
  title?: string;
  body?: string;
}

const Home: NextPageType = ({ t, i18n }) => {
  const classes = useStyles();
  const [postData, setPostData] = useState<IPost>({});

  async function getDemoData() {
    try {
      const result = await Http.Request<IPost>(
        "GET",
        `${apiConstant.API_GET_TEST}/${Math.round(Math.random() * 101)}`
      );
      setPostData(result);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    getDemoData();
  }, []);

  useEffect(() => {
    window.scrollTo(0, 0);
    i18n.changeLanguage("jp").then(() => {});
  }, [i18n]);

  return (
    <>
      <Head>
        <title>HOME PAGE</title>
      </Head>
      <div>
        <p>{t("common:home__demo_multi_language")}</p>
        <p className={classNames(classes.uppercase, classes.blue_01)}>
          home page
        </p>
        <p>user id: {postData.userId}</p>
        <p>id: {postData.id}</p>
        <p>title: {postData.title}</p>
        <p>body: {postData.body}</p>
      </div>
    </>
  );
};

Home.getInitialProps = async (): // ctx: ReduxNextPageContext
Promise<INextPageInitialProps> => {
  return Promise.all([]).then(() => {
    return { namespacesRequired: ["common"] };
  });
};

const Extended = withTranslation(["common"])(Home);

export default Extended;
