## Description: 
<!--- Copy content of Feedback to here -->

### Feedback/Bug number: 
<!--- Links to Feedback number / Gitlab Issue(s) -->

### Screenshots:
<!--- Attach a screen shot if ui change -->
<!--- or attach a gif if workflow has changed -->

### Types of changes:

<!--- Types of changes does your code introduce? Put an `x` in all the boxes that apply: -->

- [x] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to change)

### Checklist:
<!--- Go over all the following points, and put an `x` in all the boxes that apply. -->
- [ ] Screenshots (or gif) 
