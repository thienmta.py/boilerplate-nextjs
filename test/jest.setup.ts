// #region Global Imports
// @ts-ignore
import dotenv from "dotenv";
// import nock from "nock";
import { setConfig } from "next/config";
import "jest-styled-components";
import "@testing-library/jest-dom";
// #endregion Global Imports

dotenv.config({ path: ".env.test" });

setConfig({
  publicRuntimeConfig: {
    API_URL: process.env.API_URL,
    API_KEY: process.env.API_KEY,
  },
});

// nock(process.env.API_URL as string)
//   .get("/api/200")
//   .reply(200, { success: true });
//
// nock(process.env.API_URL as string)
//   .get("/api/404")
//   .reply(404, { success: false });
