// #region Global Imports
import { Provider } from "react-redux";
// #endregion Global Imports

// #region Local Imports
import { makeStore } from '@Redux';
// #endregion Local Imports

export const withRedux = () => (story: any) => (
    <Provider store={makeStore({
        pickupData: [],
    })}> {story()}</Provider >
);
